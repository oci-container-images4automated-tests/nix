defmodule Buildah.Apk.Nix do

    alias Buildah.{Apk, Apk.Catatonit, Cmd}

    def on_container(container, options) do
        user = "user"
        {uid_, 0} = Cmd.run(
            container,
            ["id", "-u", user]
        )
        {gid_, 0} = Cmd.run(
            container,
            ["id", "-g", user]
        )
        {path_, 0} = Cmd.run(container, [
            "printenv", "PATH"])
        {_, 0} = Cmd.run(container, ["env", "HOME=/home/user", "LOGNAME=user", "USER=user", "setpriv", "--reuid=#{String.trim(uid_)}", "--regid=#{String.trim(gid_)}", "--init-groups", "--",
            "printenv", "USER"])
        Apk.packages_no_cache(container, [ # Not sure if all are needed.
            "bash",
            "binutils",
            "ca-certificates",
            "coreutils",
            "file",
            "findutils",
            "man-pages",
            "mandoc",
            "moreutils",
            "openssl",
            "tar",
            "xz"
        ], options)
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v bash"])
        {cacert_, 0} = Cmd.run(container, [
            "ls", "/etc/ssl/certs/ca-certificates.crt"]) # Alpine pkg: ca-certificates-bundle
        Apk.packages_no_cache(container, [
            "--repository", "https://dl-cdn.alpinelinux.org/alpine/edge/testing",
            "nix"
        ], options)
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v nix-channel"])
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v nix-collect-garbage"])
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v nix-env"])
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v nix-store"])
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "echo 'sandbox = false' >> /etc/nix/nix.conf"])
        {_, 0} = Cmd.run(container, ["sh", "-c", # Corrects a bug in alpine nix package 2.3.5-r0
            "ls -d /nix/var/nix/profiles/default && rmdir /nix/var/nix/profiles/default"])
        {_, 0} = Cmd.run(container, [
            "nix-env", "--switch-profile", "/nix/var/nix/profiles/default"])
        # root
        # {_, 0} = Cmd.run(container, ["env", "HOME=/root", "LOGNAME=root", "USER=root",
            # "sh", "-c",
            # ". $(find / -name nix.sh -print)"])
        # {_, 0} = Cmd.run(container, ["env", "HOME=/root", "LOGNAME=root", "USER=root",
            # "sh", "-c",
            # ". /etc/profile ; printenv NIX_SSL_CERT_FILE"]) # Stored in shell config file.
        # user
        # {_, 0} = Cmd.run(container, ["env", "HOME=/home/user", "LOGNAME=user", "USER=user", "setpriv", "--reuid=#{String.trim(uid_)}", "--regid=#{String.trim(gid_)}", "--init-groups", "--",
            # "sh", "-c",
            # ". $(sudo find / -name nix.sh -print)"])
        # {_, 0} = Cmd.run(container, ["env", "HOME=/home/user", "LOGNAME=user", "USER=user", "setpriv", "--reuid=#{String.trim(uid_)}", "--regid=#{String.trim(gid_)}", "--init-groups", "--",
            # "sh", "-c",
            # ". /etc/profile ; printenv NIX_SSL_CERT_FILE"]) # Stored in shell config file.
        # nix-channel --add https://nixos.org/channels/nixos-version nixpkgs
        {_, 0} = Cmd.run(container, [
            "nix-channel", "--add", "https://nixos.org/channels/nixpkgs-unstable"])
        {_, 0} = Cmd.run(container, [
            "nix-collect-garbage", "--delete-old"])
        {_, 0} = Cmd.run(container, [
            "nix-store", "--optimise"])
        {_, 0} = Cmd.run(container, [
            "nix-store", "--verify", "--check-contents"])
        {manpath_, 0} = Cmd.run(container, [
            "ls", "-d", "/usr/share/man"])
        {env_, 0} = Cmd.run(container, [
            "ls", "/etc/profile"])
        {_, 0} = Cmd.run(container, [
            "test", "-d", "/nix/var/nix/profiles"])
        {_, 0} = Cmd.config(
            container,
            env: [
                # "ENV=" <> String.trim(env_),
                "MANPATH=" <> String.trim(manpath_),
                "PATH=/nix/var/nix/profiles/default/bin:#{String.trim(path_)}"
            ],
            into: IO.stream(:stdio, :line)
        )
    end

    def test(container, image_ID, options) do
        user = "user"
        {uid_, 0} = Cmd.run(
            container,
            ["id", "-u", user]
        )
        {gid_, 0} = Cmd.run(
            container,
            ["id", "-g", user]
        )
        {_, 0} = Podman.Cmd.run(image_ID, [
            "printenv", "USER"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )

        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v nix-channel"], into: IO.stream(:stdio, :line))
        # ...
        {_, 0} = Podman.Cmd.run(image_ID, [
            "printenv", "MANPATH"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        # {_, 0} = Podman.Cmd.run(image_ID, [
            # "printenv", "ENV"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        {_, 0} = Podman.Cmd.run(image_ID, [
            "whoami"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        # user
        # {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c",
            # ". $ENV ; printenv NIX_SSL_CERT_FILE"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # ) # . $ENV # seems needed.
        # sudo
        # {_, _} = Podman.Cmd.run(image_ID, ["sudo", "sh", "-c",
            # ". $ENV ; printenv NIX_SSL_CERT_FILE"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # ) ### Fails ######################################
        # wrong
        # {_, 0} = Podman.Cmd.run(image_ID, ["env", "HOME=/home/user", "LOGNAME=user", "USER=user", "setpriv", "--reuid=#{String.trim(uid_)}", "--regid=#{String.trim(gid_)}", "--init-groups", "--",
            # "sh", "-c",
            # ". $ENV ; printenv NIX_SSL_CERT_FILE"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c",
            "command -v nix-channel"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        # ...
    end

end
